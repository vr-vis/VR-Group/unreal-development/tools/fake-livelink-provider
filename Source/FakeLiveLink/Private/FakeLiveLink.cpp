// Copyright Epic Games, Inc. All Rights Reserved.

#include "FakeLiveLink.h"
#include "LiveLinkInterface/Public/Roles/LiveLinkTransformRole.h"
#include "LiveLinkInterface/Public/LiveLinkTypes.h"
#include "LiveLinkInterface/Public/Roles/LiveLinkTransformTypes.h"
#include "Kismet/KismetMathLibrary.h"
#include "IDisplayCluster.h"
#include "Cluster/IDisplayClusterClusterManager.h"

#define LOCTEXT_NAMESPACE "FFakeLiveLinkModule"

void FFakeLiveLinkModule::StartupModule()
{
	PositionGenerationThread = FRunnableThread::Create(this, TEXT("FakeLiveLinkProviderThread"), 0, TPri_Normal);

	FWorldDelegates::OnWorldCleanup.AddLambda([this](UWorld* World, bool bSessionEnded, bool)
	{
	    if (!World->IsGameWorld() || !bSessionEnded) return;
	
	    Running = false;
		PositionGenerationThread->WaitForCompletion();
	});
}

void FFakeLiveLinkModule::ShutdownModule(){}

bool IsMaster()
{
	if (!IDisplayCluster::IsAvailable()) 
	{
		return false;
	}
	IDisplayClusterClusterManager* Manager = IDisplayCluster::Get().GetClusterMgr();
	if (Manager == nullptr)
	{
		return false; // if we are not in cluster mode, we are always the master
	}
	return Manager->IsMaster() || !Manager->IsSlave();
}

uint32 FFakeLiveLinkModule::Run()
{
	if(!IsMaster()) return EXIT_SUCCESS;

	/* Register LiveLink Provider */
	LiveLinkProvider = ILiveLinkProvider::CreateLiveLinkProvider(TEXT("FakeProvider"));

	/* Set Subject Static Data */
	FLiveLinkStaticDataStruct static_data(FLiveLinkTransformStaticData::StaticStruct());
	LiveLinkProvider->UpdateSubjectStaticData(SubjectName, ULiveLinkTransformRole::StaticClass(), MoveTemp(static_data) );

    FDateTime OldTime = FDateTime::UtcNow();
	FVector Position = FVector::ForwardVector * RingSize;
	FRotator Rotation = UKismetMathLibrary::MakeRotFromXZ(FVector::BackwardVector, FVector::UpVector);

	while(Running)
	{
        double DeltaTime = (FDateTime::UtcNow() - OldTime).GetTotalSeconds();
		OldTime = FDateTime::UtcNow();

		Position = Position.RotateAngleAxis(DeltaTime*DegreePerSecond, FVector::UpVector);
		Rotation += UKismetMathLibrary::RotatorFromAxisAndAngle(FVector::UpVector,DeltaTime*DegreePerSecond);

	    FLiveLinkFrameDataStruct FrameData(FLiveLinkTransformFrameData::StaticStruct());
	    FLiveLinkTransformFrameData* TransformData = FrameData.Cast<FLiveLinkTransformFrameData>();
	    TransformData->Transform.SetLocation(Position);
	    TransformData->Transform.SetRotation(Rotation.Quaternion());
	    TransformData->Transform.SetScale3D(FVector(1.0f, 1.0f, 1.0f));

		LiveLinkProvider->UpdateSubjectFrameData(SubjectName, MoveTemp(FrameData));

		FPlatformProcess::Sleep(1.0/UpdateRate);
	}

	return EXIT_SUCCESS;
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FFakeLiveLinkModule, FakeLiveLink)