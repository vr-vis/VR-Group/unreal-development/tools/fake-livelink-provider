// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HAL/Runnable.h"
#include "HAL/RunnableThread.h"
#include "LiveLinkMessageBusFramework/Public/LiveLinkProvider.h"
#include "Modules/ModuleManager.h"

class FFakeLiveLinkModule : public IModuleInterface, public FRunnable
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/* Threading */
	FRunnableThread* PositionGenerationThread = nullptr;
	uint32 Run() override;
	bool Running = true;

	/* Generated Path */
	float RingSize = 100;
	float DegreePerSecond = 20;

	/* LiveLink Parameters */
	double UpdateRate = 100.0f; /* UpdateRate in Hz */
	const FName SubjectName = TEXT("RingRotation");
	TSharedPtr<ILiveLinkProvider> LiveLinkProvider;
};
